# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:
#
# This CI uses the freedesktop.org ci-templates.
# Please see the ci-templates documentation for details:
# https://freedesktop.pages.freedesktop.org/ci-templates/

.templates_sha: &template_sha 185ede0e9b9b1924b92306ab8b882a6294e92613 # see https://docs.gitlab.com/ee/ci/yaml/#includefile


include:
  # Debian container builder template
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/debian.yml'
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/ci-fairy.yml'
  - template: Security/SAST.gitlab-ci.yml


stages:
  - prep             # prep work like rebuilding the container images if there is a change
  - install macros
  - build            # for actually building and testing things in a container
  - test
  - deploy


variables:
  FDO_UPSTREAM_REPO: 'xorg/doc/xorg-sgml-doctools'
  FDO_DISTRIBUTION_VERSION: 'stable'
  # The tag should be updated each time the list of packages is updated.
  # Changing a tag forces the associated image to be rebuilt.
  # Note: the tag has no meaning, we use a date format purely for readability
  FDO_DISTRIBUTION_TAG:  '2023-11-19.0'
  FDO_DISTRIBUTION_PACKAGES:  'git pkgconf autoconf automake make xz-utils xutils-dev xmlto w3m xsltproc fop meson ninja-build jq'


#
# Verify that commit messages are as expected
#
check-commits:
  extends:
    - .fdo.ci-fairy
  stage: prep
  script:
    - ci-fairy check-commits --junit-xml=results.xml
  except:
    - master@xorg/doc/xorg-sgml-doctools
  variables:
    GIT_DEPTH: 100
  artifacts:
    reports:
      junit: results.xml

#
# Verify that the merge request has the allow-collaboration checkbox ticked
#
check-merge-request:
  extends:
    - .fdo.ci-fairy
  stage: deploy
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=results.xml
  artifacts:
    when: on_failure
    reports:
      junit: results.xml
  allow_failure: true


#
# Build a container with the given tag and the packages pre-installed.
# This only happens if/when the tag changes, otherwise the existing image is
# re-used.
#
container-prep:
  extends:
    - .fdo.container-build@debian
  stage: prep
  variables:
    GIT_STRATEGY: none

# Install latest version of xorg-macros, since debian doesn't package 1.20.0 yet
install-macros:
  extends:
    - .fdo.distribution-image@debian
  stage: install macros
  script:
    - export INSTDIR="$PWD/_inst"
    - git clone --depth=1 https://gitlab.freedesktop.org/xorg/util/macros
    - pushd macros > /dev/null
    - autoreconf -ivf
    - mkdir _builddir
    - pushd _builddir > /dev/null
    - ../configure --disable-silent-rules --prefix="$INSTDIR"
    - make
    - make install
    - popd > /dev/null
    - popd > /dev/null
  variables:
  artifacts:
    paths:
      - _inst


#
# The default build, runs on the image built above.
# Sets ACLOCAL to use macros from "install macros" stage above.
#
autotools:
  stage: build
  extends:
    - .fdo.distribution-image@debian
  script:
    - export INSTDIR="$PWD/_inst"
    - export ACLOCAL="aclocal -I $INSTDIR/share/aclocal"
    - autoreconf -ivf
    - mkdir _builddir
    - pushd _builddir > /dev/null
    - ../configure --disable-silent-rules --prefix="$INSTDIR"
    - make
    - make check
    - make install
    - make distcheck
    - mv xorg-sgml-doctools-*.tar.xz ..
    - popd > /dev/null
  variables:
  artifacts:
    paths:
      - _inst
      - xorg-sgml-doctools-*.tar.xz

#
# Make sure xorg-docs builds with the xorg-sgml-doctools package built above
# Sets ACLOCAL to use macros from "install macros" stage above.
#
docs-build:
  extends:
    - .fdo.distribution-image@debian
  stage: test
  script:
    - export INSTDIR="$PWD/_inst"
    - export ACLOCAL="aclocal -I $INSTDIR/share/aclocal"
    - export PKG_CONFIG_PATH=$(find $INSTDIR/ -name '*.pc' -printf "%h:")
    - git clone --depth=1 https://gitlab.freedesktop.org/xorg/doc/xorg-docs
    - pushd xorg-docs > /dev/null
    - autoreconf -ivf
    - mkdir _builddir
    - pushd _builddir > /dev/null
    - ../configure --disable-silent-rules --prefix="$INSTDIR"
    - make
    - make check
    - make install
    - make distcheck
    - popd > /dev/null
    - popd > /dev/null
  variables:
  artifacts:
    paths:
      - _inst

meson:
  extends:
    - .fdo.distribution-image@debian

  stage: build
  script:
    - mkdir -p ../_inst
    - meson setup builddir --prefix="$PWD/../_inst"
    - meson install -C builddir

meson from tarball:
  extends:
    - .fdo.distribution-image@debian

  stage: test
  script:
    - mkdir -p _tarball_build
    - tar xf xorg-sgml-doctools-*.tar.xz -C _tarball_build
    - pushd _tarball_build/xorg-sgml-doctools-*
    - meson setup builddir
    - meson install -C builddir
  needs:
    - autotools
  variables:
    GIT_STRATEGY: none

compare meson and autotools:
  extends:
    - .fdo.distribution-image@debian

  stage: test
  script:
    - mkdir -p $PWD/_meson_inst
    - mkdir -p $PWD/_autotools_inst
    - meson setup builddir --prefix=/usr/X11
    - DESTDIR=$PWD/_meson_inst meson install -C builddir
    - export INSTDIR="$PWD/_inst"
    - export ACLOCAL="aclocal -I $INSTDIR/share/aclocal"
    - autoreconf -ivf
    - ./configure --prefix=/usr/X11
    - make && DESTDIR=$PWD/_autotools_inst make install
    - diff --brief --recursive $PWD/_meson_inst $PWD/_autotools_inst

check versions are in sync:
  extends:
    - .fdo.distribution-image@debian

  stage: test
  script:
    - export INSTDIR="$PWD/_inst"
    - export ACLOCAL="aclocal -I $INSTDIR/share/aclocal"
    - autoreconf -ivf
    - ./configure --version | head -n 1 | sed -e 's/xorg-sgml-doctools configure //' > autotools.version
    - meson introspect meson.build --projectinfo | jq -r '.version' > meson.version
    - diff -u autotools.version meson.version || (echo "ERROR - autotools and meson versions not in sync" && false)
